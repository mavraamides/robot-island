﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace GOAP
{
    public class Node
    {
        public Node parent;
        public float cost;
        public Dictionary<GState, int> state;
        public GAction action;

        public Node( Node parent, float cost, Dictionary<GState, int> allstates, GAction action )
        {
            this.parent = parent;
            this.cost = cost;
            this.state = new Dictionary<GState, int>( allstates );
            this.action = action;
        }
        public Node( Node parent, float cost, Dictionary<GState, int> allstates, Dictionary<GState, int> beliefstates, GAction action )
        {
            this.parent = parent;
            this.cost = cost;
            this.state = new Dictionary<GState, int>( allstates );

            foreach (KeyValuePair<GState, int> kvp in beliefstates)
            {
                if ( !this.state.ContainsKey( kvp.Key ) ) this.state.Add( kvp.Key, kvp.Value );
            }
            this.action = action;
        }
    }


    public class GPlanner
    {
        public Queue<GAction> Plan( List<GAction> actions, Dictionary<GState, int> goal, BlackBoard beliefeStates )
        {
            List<GAction> usableActions = new List<GAction>();
            foreach ( GAction a in actions )
            {
                if ( a.IsAchievable() ) usableActions.Add( a );
            }

            List<Node> leaves = new List<Node>();
            Node start = new Node(null, 0, GWorld.Instance.GetBlackboard().GetStates(), beliefeStates.GetStates(), null);

            bool success = BuildGraph(start, leaves, usableActions, goal);

            if ( !success )
            {
                Debug.Log( "NO PLAN" );
                return null;
            }

            Node cheapest = null;
            foreach ( Node leaf in leaves )
            {
                if ( cheapest == null )
                {
                    cheapest = leaf;
                }
                else
                {
                    if ( leaf.cost < cheapest.cost )
                    {
                        cheapest = leaf;
                    }
                }
            }

            List<GAction> result = new List<GAction>();
            Node n = cheapest;

            while ( n != null )
            {
                if ( n.action != null )
                {
                    result.Insert( 0, n.action );
                }
                n = n.parent;
            }

            Queue<GAction> queue = new Queue<GAction>();
            foreach ( GAction a in result )
            {
                queue.Enqueue( a );
            }
            
            Debug.Log( "The plan is: " );
            foreach ( GAction a in queue ) Debug.Log( "Q: " + a.GetType().Name );
            
            return queue;

        }

        private bool BuildGraph( Node parent, List<Node> leaves, List<GAction> usableActions, Dictionary<GState, int> goal )
        {
            bool foundPath = false;
            foreach ( GAction action in usableActions )
            {
                if ( action.IsAchievableGiven( parent.state ) )
                {
                    Dictionary<GState, int> currentState = new Dictionary<GState, int>(parent.state);
                    foreach ( KeyValuePair<GState, int> eff in action.effects )
                    {
                        if ( !currentState.ContainsKey( eff.Key ) )
                        {
                            currentState.Add( eff.Key, eff.Value );
                        }
                    }
                    Node node = new Node(parent, parent.cost + action.Cost, currentState, action);

                    if ( GoalAchieved( goal, currentState ) )
                    {
                        leaves.Add( node );
                        foundPath = true;
                    }
                    else
                    {
                        List<GAction> subset = ActionSubset(usableActions, action);
                        bool found = BuildGraph(node, leaves, subset, goal);
                        if ( found ) foundPath = true;
                    }

                }
            }

            return foundPath;
        }

        private List<GAction> ActionSubset( List<GAction> actions, GAction removeMe )
        {
            List<GAction> subset = new List<GAction>();
            foreach ( GAction a in actions )
            {
                if ( !a.Equals( removeMe ) )
                {
                    subset.Add( a );
                }
            }
            return subset;
        }

        private bool GoalAchieved( Dictionary<GState, int> goal, Dictionary<GState, int> state )
        {
            foreach ( KeyValuePair<GState, int> g in goal )
            {
                if ( !state.ContainsKey( g.Key ) ) return false;
            }
            return true;
        }


    }
}
