﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GOAP
{
    public class WhiteBoard
    {
        private Dictionary<GState, Queue<GameObject>> resources;

        public WhiteBoard()
        {
            resources = new Dictionary<GState, Queue<GameObject>>();
        }

        public void AddResource( GState key, GameObject resource)
        {
            if ( !resources.ContainsKey( key ) ) resources[key] = new Queue<GameObject>();
            resources[key].Enqueue( resource );
        }

        public GameObject RemoveResource( GState key )
        {
            if ( GetCount( key ) == 0 ) return null;
            return resources[key].Dequeue();
        }

        public void RemoveResource( GState key, GameObject resource)
        {
            resources[key] = new Queue<GameObject>( resources[key].Where( p => p != resource ) );
        }

        public int GetCount( GState key )
        {
            if ( !resources.ContainsKey( key ) ) return 0;
            return resources[key].Count;
        }
    }
}
