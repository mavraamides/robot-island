﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GOAP
{
    public sealed partial class GWorld
    {
        private static readonly GWorld instance = new GWorld();
        private static BlackBoard blackBoard;
        private static WhiteBoard whiteBoard;

        static GWorld()
        {
            blackBoard = new BlackBoard();
            whiteBoard = new WhiteBoard();

            Time.timeScale = 5;
        }

        public void AddToWhiteboard( GState key, GameObject resource )
        {
            whiteBoard.AddResource( key, resource );
            blackBoard.SetState( key, whiteBoard.GetCount( key ) );
        }

        public void RemoveFromWhiteBoard( GState key, GameObject resource )
        {
            whiteBoard.RemoveResource( key, resource );
            blackBoard.SetState( key, whiteBoard.GetCount( key ) );
        }

        public GameObject RemoveFromWhiteBoard( GState key )
        {
            GameObject resource = whiteBoard.RemoveResource(key);
            blackBoard.SetState( key, whiteBoard.GetCount(key) );
            return resource;
        }

        public static GWorld Instance
        {
            get { return instance; }
        }

        public BlackBoard GetBlackboard()
        {
            return blackBoard;
        }
    }
}
