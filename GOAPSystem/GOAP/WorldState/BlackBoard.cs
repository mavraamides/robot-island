﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GOAP
{
    [System.Serializable]
    public class WorldState
    {
        public GState key;
        public int value;
    }

    public class BlackBoard
    {
        public Dictionary<GState, int> states;

        public BlackBoard()
        {
            states = new Dictionary<GState, int>();
        }

        public bool HasState( GState key )
        {
            return states.ContainsKey( key );
        }

        public int GetStateCount( GState key )
        {
            if ( HasState( key ) ) return states[key];
            return 0;
        }

        public void ModifyState( GState key, int val)
        {
            if ( HasState( key))
            {
                states[key] += val;
            }
            else
            {
                states.Add( key, val );
            }
            if ( states[key] <= 0 ) RemoveState( key );
        }

        public void RemoveState( GState key )
        {
            if ( HasState( key ) ) states.Remove( key );
        }

        public void SetState( GState key, int val)
        {
            if (HasState(key))
            {
                states[ key] = val;
            }
            else
            {
                states.Add( key, val );
            }
            if ( states[key] <= 0 ) RemoveState( key );
        }

        public Dictionary<GState, int> GetStates()
        {
            return states;
        }
    }
}
