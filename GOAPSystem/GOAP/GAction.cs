﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace GOAP
{
    public abstract class GAction : MonoBehaviour
    {
        public GameObject target;

        // These attributes must be overwriteen in OverrideActiontSpecificData
        protected float cost = 1.0f;
        protected string targetTag;
        protected float duration;
        public Dictionary<GState, int> preconditions;
        public Dictionary<GState, int> effects;
        // End of overwriteable attributes

        public float Cost { get { return cost; } }
        public string TargetTag { get { return targetTag; } }
        public float Duration { get { return duration; } }

        public GInventory inventory;
        public BlackBoard beliefs;
        protected bool running = false;
        protected NavMeshAgent agent;

        public bool Running { get { return running; } set { running = value; } }
        public NavMeshAgent Agent { get { return agent; } }

        public GAction()
        {
            preconditions = new Dictionary<GState, int>();
            effects= new Dictionary<GState, int>();
        }

        private void Awake()
        {
            OverrideActiontSpecificData();
            agent = this.gameObject.GetComponent<NavMeshAgent>();

            inventory = this.GetComponent<GAgent>().inventory;
            beliefs = this.GetComponent<GAgent>().beliefs;
        }


        public bool IsAchievable()
        {
            return true;
        }

        public bool IsAchievableGiven(Dictionary<GState, int> conditions)
        {
            foreach (KeyValuePair<GState, int> kvp in preconditions)
            {
                if ( !conditions.ContainsKey( kvp.Key ) ) return false;
            }
            return true;
        }

        public bool TransferFromWhiteBoardToInventory( GState key )
        {
            target = GWorld.Instance.RemoveFromWhiteBoard( key );
            if ( target == null ) return false;
            inventory.AddItem( target );
            return true;
        }

        public void TransferFromInventoryToWhiteBoard( GState key )
        {
            if ( target != null )
            {
                inventory.RemoveItem( target );
                GWorld.Instance.AddToWhiteboard( key, target );
                target = null;
            }
        }

        public abstract void OverrideActiontSpecificData();
        public abstract bool PrePerform();
        public abstract bool PostPerform();
    }
}
