﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace GOAP
{
    public class SubGoal
    {
        public Dictionary<GState, int> sgoals;
        public bool remove;

        public SubGoal( GState state, int cost, bool remove)
        {
            sgoals = new Dictionary<GState, int>();
            sgoals.Add( state, cost );
            this.remove = remove;
        }
    }

    public class GAgent : MonoBehaviour
    {
        public List<GAction> actions = new List<GAction>();
        public Dictionary<SubGoal, int> goals = new Dictionary<SubGoal, int>();
        public GInventory inventory = new GInventory();
        public BlackBoard beliefs = new BlackBoard();
        public float throttle = 0.5f;

        private float timeSinceLastPlan;
        GPlanner planner;
        Queue<GAction> actionQueue;
        public GAction currentAction;
        SubGoal currentGoal;
        Vector3 destination = Vector3.zero;

        public void Start()
        {
            GAction[] acts = this.GetComponents<GAction>();
            foreach(GAction a in acts)
            {
                actions.Add( a );
            }
            timeSinceLastPlan = throttle;
        }

        bool invoked = false;
        void CompleteAction()
        {
            currentAction.Running = false;
            currentAction.PostPerform();
            invoked = false;
        }

        virtual public void BeforePlan()
        {

        }

        // TODO: Move all navmesh stuff to lower level namespace
        void LateUpdate()
        {
            BeforePlan();
            timeSinceLastPlan -= Time.deltaTime;
            if ( timeSinceLastPlan > 0f ) return;
            timeSinceLastPlan = throttle;

            if (currentAction != null && currentAction.Running)
            {
                float remainingDistance = Vector3.Distance(destination, transform.position);
                // TODO: Make minimum distance action or agnt specific
                if ( remainingDistance < 2f )
                {
                    if (!invoked )
                    {
                        Invoke( "CompleteAction", currentAction.Duration );
                        invoked = true;
                    }
                }
                return;
            }

            if (planner == null || actionQueue == null)
            {
                planner = new GPlanner();

                var sortedGoals = from entry in goals orderby entry.Value descending select entry;

                foreach (KeyValuePair<SubGoal, int> sg in sortedGoals)
                {
                    actionQueue = planner.Plan( actions, sg.Key.sgoals, beliefs );
                    if (actionQueue != null)
                    {
                        currentGoal = sg.Key;
                        break;
                    }
                }
            }

            if (actionQueue != null && actionQueue.Count == 0)
            {
                if ( currentGoal.remove ) goals.Remove( currentGoal );
                planner = null;
            }

            if (actionQueue != null && actionQueue.Count > 0)
            {
                currentAction = actionQueue.Dequeue();
                if (currentAction.PrePerform())
                {
                    if (currentAction.target == null && currentAction.TargetTag != "")
                    {
                        currentAction.target = GameObject.FindWithTag( currentAction.TargetTag );
                    }

                    if (currentAction.target != null)
                    {
                        currentAction.Running = true;
                        destination = currentAction.target.transform.position;

                        // Are these next two lines even used anymore?
                        Transform dest = currentAction.target.transform.Find("Destination");
                        if ( dest != null )
                        {
                            Debug.LogWarning( "NOTE: Found Destination in target." );
                            destination = dest.position;
                        }
                        currentAction.Agent.SetDestination( destination );
                    }
                }
                else
                {
                    actionQueue = null;
                }
            }
        }
    }
}
