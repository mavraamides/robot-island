﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOAP
{
    public class GStateMonitor : MonoBehaviour
    {
        public GState state;
        public float stateStrength;
        public float stateDecayRate;
        public BlackBoard beliefs;
        public GameObject resourcePrefab;
        public GAction action;

        bool stateFound = false;
        float initialStrength = 0f;

        void Awake()
        {
            beliefs = this.GetComponent<GAgent>().beliefs;
            initialStrength = stateStrength;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (action.Running)
            {
                stateFound = false;
                stateStrength = initialStrength;
            }

            if (!stateFound && beliefs.HasState(state))
            {
                stateFound = true;
            }

            if (stateFound)
            {
                stateStrength -= stateDecayRate * Time.deltaTime;
                if (stateStrength <= 0)
                {
                    Vector3 location = new Vector3( this.transform.position.x, resourcePrefab.transform.position.y, this.transform.position.z );
                    GameObject p = Instantiate(resourcePrefab, location, resourcePrefab.transform.rotation);
                    stateFound = false;
                    stateStrength = initialStrength;
                    beliefs.RemoveState( state );
                }
            }
        }
    }
}
