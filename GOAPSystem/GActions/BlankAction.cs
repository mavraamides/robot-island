﻿using GOAP;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOAP
{
    public class BlankAction : GAction
    {
        public override bool PrePerform()
        {
            return true;
        }

        public override bool PostPerform()
        {
            return true;
        }

        public override void OverrideActiontSpecificData()
        {
            Debug.Log( "OverrideActiontSpecificData for " + this.GetType().Name );
            //targetTag = "";
            //duration = 1f;
            //preconditions[GState.None] = 0;
            //effects[GState.None] = 0;
        }
    }
}
