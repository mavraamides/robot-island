﻿using GOAP;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateWorld : MonoBehaviour
{
    public Text states;

    // Update is called once per frame
    void LateUpdate()
    {
        Dictionary<GState, int> worldstates = GWorld.Instance.GetBlackboard().GetStates();
        states.text = "";
        foreach(KeyValuePair<GState, int> s in worldstates)
        {
            states.text += s.Key + ", " + s.Value + "\n";
        }
    }
}
