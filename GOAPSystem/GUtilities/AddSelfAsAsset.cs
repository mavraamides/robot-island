﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GOAP;

public class AddSelfAsAsset : MonoBehaviour
{
    public GState state;
 
    // Start is called before the first frame update
    void Awake()
    {
        GWorld.Instance.AddToWhiteboard( state, gameObject );
    }

    private void OnDestroy()
    {
        GWorld.Instance.RemoveFromWhiteBoard( state, gameObject );
    }
}
